package pl.jsystems.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibrarianLauncher {

    public static void main(String[] args) {
        SpringApplication.run(LibrarianLauncher.class, args);
    }
}
