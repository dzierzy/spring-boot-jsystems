package pl.jsystems.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.jsystems.spring.dao.PublishersDAO;

@Controller
@RequestMapping("publishersWeb")
public class PublishersController {


    @Autowired
    PublishersDAO publishersDAO;

    @GetMapping
    public String getAllPublishers(Model model){

        model.addAttribute("publishers", publishersDAO.findAll());
        return "publishers";

    }
}
