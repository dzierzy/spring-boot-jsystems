package pl.jsystems.spring.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.jsystems.spring.model.Book;
import pl.jsystems.spring.model.Publisher;

import java.util.List;

public interface BooksDAO extends JpaRepository<Book, Long> {


    //@Query("SELECT b FROM Book b where b.publisher=:p")
    List<Book> findAllByPublisher(/*@Param("p")*/ Publisher p);

    @Query("SELECT b FROM Book b where b.publisher.id=:id")
    List<Book> findAllByPublisherId(@Param("id") Long id);


}
