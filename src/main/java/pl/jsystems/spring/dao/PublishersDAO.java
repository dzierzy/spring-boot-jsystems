package pl.jsystems.spring.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import pl.jsystems.spring.model.Publisher;

public interface PublishersDAO extends JpaRepository<Publisher, Long> {




}
