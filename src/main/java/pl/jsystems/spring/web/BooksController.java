package pl.jsystems.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.jsystems.spring.dao.BooksDAO;

@Controller
@RequestMapping("booksWeb")
public class BooksController {


    @Autowired
    BooksDAO booksDAO;

    @GetMapping
    String getBooksByPublisher(@RequestParam("publisherId") Long publisherId, Model model){
        model.addAttribute("books", booksDAO.findAllByPublisherId(publisherId));
        return "books";
    }
}
